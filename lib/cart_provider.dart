import 'dart:collection';

import 'package:flutter/material.dart';

class CartProvider extends ChangeNotifier {
  /// Internal, private state of the cart.
  final List<CartItem> _items = [];

  /// An unmodifiable view of the items in the cart.
  UnmodifiableListView<CartItem> get items => UnmodifiableListView(_items);

  /// The current total price of all items (assuming all items cost $42).
  // int get totalPrice => _items.length * 8;

  double get totalPrice {
    double total = 0;
    for ( var cartItem in _items) {
      total += cartItem.price * cartItem.quantity;
    }
    return total;
  }

  /// Adds [item] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(CartItem item) {
    
    // var matchingItems = items.where((item) => item.name == item.name).indexed;
    // print(matchingItems);


    _items.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  /// Removes all items from the cart.
  void removeAll() {
    _items.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
    
}

class CartItem{
  final String name;
  final double price;
  final String imageId;
  final int quantity;

  CartItem({
    required this.name,
    required this.price,
    required this.imageId,
    required this.quantity,
  });

}
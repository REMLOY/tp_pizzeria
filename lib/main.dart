import 'package:flutter/material.dart';
import 'package:pizza/pizza.dart';
import 'package:pizza/cart_provider.dart';
import 'package:provider/provider.dart';

void main() async {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CartProvider()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Pizzeria API';
    return const MaterialApp(
      title: appTitle,
      home: Pizza(),
    );
  }
}

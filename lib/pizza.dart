import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:pizza/cart_provider.dart';
import 'package:pizza/routes.dart';
import 'package:provider/provider.dart';

class Pizza extends StatelessWidget {
  const Pizza({super.key});

  @override
  Widget build(BuildContext context) {
    final dio = Dio();

    void showPizzaInfo(BuildContext context, PizzaInfo pizzaInfo) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(pizzaInfo.name),
            content: Scaffold(
              appBar: AppBar(
                title: const Text('Ingredients'),
              ),
              body: ListView.builder(
                  itemCount: pizzaInfo.ingredients.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        ListTile(
                          title: Text(pizzaInfo.ingredients[index] as String),
                        )
                      ],
                    );
                  }),
            ),
          );
        },
      );
    }

    Future<List<PizzaInfo>> getHttp(link) async {
      final response = await dio.get(link);
      if (response.statusCode == 200) {
        return response.data['data']
            .map<PizzaInfo>((json) => PizzaInfo.fromJson(json))
            .toList();
      } else {
        throw Exception("Can't load pizzas");
      }
    }

    return Scaffold(
      appBar: AppBar(title: const Text('Menu Pizza'), actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const CartMenu(),
              ),
            );
          },
          child: const Text(
            'Cart',
            style: TextStyle(color: Colors.black),
          ),
        )
      ]),
      body: Consumer<CartProvider>(builder: (context, cart, child) {
        return FutureBuilder(
          future: getHttp('https://pizzas.shrp.dev/items/pizzas'),
          builder: ((context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(children: [
                      ListTile(
                        leading: Image.network(
                          'https://pizzas.shrp.dev/assets/${snapshot.data![index].imageId}',
                          width: 50,
                          height: 50,
                          fit: BoxFit.cover,
                        ),
                        title: Text(snapshot.data![index].name),
                        subtitle: Text(
                            'Price: ${snapshot.data![index].price.toString()}'
                            ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                showPizzaInfo(context, snapshot.data![index]);
                              },
                              child: const Text("detail"))
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                cart.add(CartItem(name: snapshot.data![index].name, price: snapshot.data![index].price, imageId: snapshot.data![index].imageId, quantity: 1));
                              },
                              child: const Text("+"))
                        ],
                      ),
                    ]);
                  });
            } else if (snapshot.hasError) {
              print(snapshot.error);
              return const Text("Erreur");
            } else {
              return const CircularProgressIndicator();
            }
          }),
        );
      }),
    );
  }
}

class PizzaInfo {
  String name;
  List ingredients;
  double price;
  String imageId;

  PizzaInfo({
    required this.name,
    required this.ingredients,
    required this.price,
    required this.imageId,
  });

  factory PizzaInfo.fromJson(Map<String, dynamic> json) {
    return PizzaInfo(
      name: json['name'] as String,
      ingredients: json['ingredients'] as List,
      price: json['price'] as double,
      imageId: json['image'] as String,
    );
  }
}

class IngredientInfo {
  String name;
  String imageId;

  IngredientInfo({
    required this.name,
    required this.imageId,
  });

  factory IngredientInfo.fromJson(Map<String, dynamic> json) {
    return IngredientInfo(
      name: json['name'] as String,
      imageId: json['image'] as String,
    );
  }
}

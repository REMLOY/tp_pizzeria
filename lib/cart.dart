import 'package:flutter/material.dart';
import 'package:pizza/cart_provider.dart';
import 'package:provider/provider.dart';

class Cart extends StatelessWidget {
  const Cart({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, cart, child) {
      ElevatedButton(
        onPressed: () {
          Navigator.pushNamed(context, '/cart');
        },
        child: const Text('Go back!'),
      );
      return Scaffold(
          body: Text('Total price: ${cart.totalPrice}')
          );
    });
  }
}

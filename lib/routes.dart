import 'package:flutter/material.dart';
import 'package:pizza/cart.dart';
import 'package:pizza/pizza.dart';

class CartMenu extends StatelessWidget {
  const CartMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Votre panier'),
      ),
      body: const Cart(),
    );
  }
}

class PizzasMenu extends StatelessWidget {
  const PizzasMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Menu des pizzas'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          child: const Pizza(),
        ),
      ),
    );
  }
}